-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 27, 2018 at 11:48 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `site`
--

-- --------------------------------------------------------

--
-- Table structure for table `ass.produits-comcli`
--

DROP TABLE IF EXISTS `ass.produits-comcli`;
CREATE TABLE IF NOT EXISTS `ass.produits-comcli` (
  `asso.FKComCli` int(11) NOT NULL,
  `asso.FKProduit` int(11) NOT NULL,
  `quantite-asso.Produits-comcli` int(11) NOT NULL,
  PRIMARY KEY (`asso.FKComCli`,`asso.FKProduit`),
  KEY `asso.FK` (`asso.FKComCli`,`asso.FKProduit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `commandeclient`
--

DROP TABLE IF EXISTS `commandeclient`;
CREATE TABLE IF NOT EXISTS `commandeclient` (
  `IdComCli` int(11) NOT NULL AUTO_INCREMENT,
  `comCli-idUser` int(11) NOT NULL,
  `dateComCli` date NOT NULL,
  `heureComCli` time NOT NULL,
  PRIMARY KEY (`IdComCli`),
  KEY `comCli-idUser` (`comCli-idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `endroits`
--

DROP TABLE IF EXISTS `endroits`;
CREATE TABLE IF NOT EXISTS `endroits` (
  `idEndroits` int(11) NOT NULL AUTO_INCREMENT,
  `endroits-FKCommande` int(11) NOT NULL,
  `endroits-FKFournisseur` int(11) NOT NULL,
  `adressesEndroits` varchar(40) NOT NULL,
  `npaEndroits` int(11) NOT NULL,
  `localiteEndroits` varchar(20) NOT NULL,
  PRIMARY KEY (`idEndroits`),
  KEY `adresse-PKCommande` (`endroits-FKCommande`),
  KEY `endroit-FKFournisseur` (`endroits-FKFournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `idFournisseur` int(11) NOT NULL AUTO_INCREMENT,
  `NomFournisseur` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`idFournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `idProduits` int(11) NOT NULL AUTO_INCREMENT,
  `descriptionProduits` varchar(40) CHARACTER SET utf8 NOT NULL,
  `prixProduits` decimal(10,2) NOT NULL,
  `typeProduits` varchar(40) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`idProduits`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produits`
--

INSERT INTO `produits` (`idProduits`, `descriptionProduits`, `prixProduits`, `typeProduits`) VALUES
(1, 'Hotdog', '9.50', 'nourriture'),
(2, 'Hamburger', '10.00', 'nourriture'),
(3, 'CocaCola', '3.50', 'boisson'),
(4, 'Frites', '4.50', 'garniture'),
(5, 'Patatoes', '5.00', 'garniture'),
(6, 'Pizza', '11.00', 'nourriture'),
(7, 'Ice-Tea', '3.50', 'boisson'),
(8, 'Sprite', '3.50', 'boisson');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `IdUsers` int(11) NOT NULL AUTO_INCREMENT,
  `NomUsers` varchar(20) CHARACTER SET utf8 NOT NULL,
  `PrenomUsers` varchar(20) CHARACTER SET utf8 NOT NULL,
  `AdresseUsers` varchar(80) CHARACTER SET utf8 NOT NULL,
  `CodePostalUsers` varchar(4) CHARACTER SET utf8 NOT NULL,
  `LocaliteUsers` varchar(20) CHARACTER SET utf8 NOT NULL,
  `EmailUsers` varchar(40) CHARACTER SET utf8 NOT NULL,
  `LoginUsers` varchar(40) CHARACTER SET utf8 NOT NULL,
  `MdpUsers` varchar(60) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`IdUsers`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`IdUsers`, `NomUsers`, `PrenomUsers`, `AdresseUsers`, `CodePostalUsers`, `LocaliteUsers`, `EmailUsers`, `LoginUsers`, `MdpUsers`) VALUES
(2, 'Norris', 'Chuckchuck', 'chemin35', '1219', 'Geneve', 'chuck@gmail.com', 'chuckin', 'ch'),
(3, 'Norris', 'Chuck', 'chemin3', '1218', 'Geneve', 'chuck@gmail.com', 'chuckin', 'dd'),
(4, 'Norris', 'Chuck', 'chemin3', '1218', 'Geneve', 'chuck@gmail.com', 'chuckin2', 'w'),
(5, 'papa', 'a', 'chemin', '1218', 'Geneve', 'p@gmail.com', 'pap', 'p'),
(6, 'gag', 'sgag', 'aa', '1218', 'dd', 'chuck@gmail.com', 'dd', 'aa'),
(7, 'gag', 'sgag', 'aa', '1218', 'dd', 'chuck@gmail.com', 'd', 'ss'),
(8, 'gag', 'sgag', 'aa', '1218', 'dd', 'chuck@gmail.com', 'q', 'ss'),
(9, 'gag', 'sgag', 'aa', '1218', 'dd', 'chuck@gmail.com', 'e', 'a'),
(10, 'gag', 'sgag', 'aa', '1218', 'dd', 'chuck@gmail.com', 'c', 'c');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commandeclient`
--
ALTER TABLE `commandeclient`
  ADD CONSTRAINT `commandeclient_ibfk_1` FOREIGN KEY (`comCli-idUser`) REFERENCES `commandeclient` (`IdComCli`);

--
-- Constraints for table `endroits`
--
ALTER TABLE `endroits`
  ADD CONSTRAINT `endroits_ibfk_1` FOREIGN KEY (`endroits-FKFournisseur`) REFERENCES `fournisseur` (`idFournisseur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
