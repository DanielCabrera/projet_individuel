<?php session_start(); ?>
<!DOCTYPE html>

<html>
	<head>


		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/bootstrap.min.css"/>

		<title>Nouveau document HTML 5</title>
	</head>

	<body>
		<?php
			$id = $_SESSION['IdUsers'];

			try {
					$bdd = new PDO("mysql:host=localhost;dbname=site", "root", "");
					$bdd->query("SET NAMES 'utf8'");
					$reponse =$bdd->query("SELECT * FROM user WHERE IdUsers = '$id'" );
					$reponse->setFetchMode(PDO::FETCH_BOTH);

					$donnees=$reponse->fetch();

					?>
						<form action="ConsultModifierDonneesPerso.php" method="post">
							<p>
							<fieldset>
								<legend><h1>Modifier vos données personnelles</h1></legend>
								<h3>Tous les champs sont obligatoires</h3>

								<label for="nom">Votre nom</label></br>
								<input type="text" name="nom" id="nom" required autofocus  size="20" maxlength="20" value="<?php echo $donnees['NomUsers']; ?>"/></br>

								<label for="prenom">Votre prénom:</label></br>
								<input type="text" name="prenom" id="prenom" required  size="20" maxlength="20" value="<?php echo $donnees['PrenomUsers']; ?>"/></br>

								<label for="adresse">Votre adresse :</label></br>
								<input type="text" name="adresse" id="adresse" required size="80" maxlength="80" value="<?php echo $donnees['AdresseUsers']; ?>"></br>

								<label for="codePostal">Votre code postal:</label></br>
								<input type="text" name="codePostal" id="codePostal" required  size="4" maxlength="4" value="<?php echo $donnees['CodePostalUsers']; ?>"/></br>

								<label for="localite">Votre localité</label></br>
								<input type="text" name="localite" id="localite" required size="20" maxlength="20" value="<?php echo $donnees['LocaliteUsers']; ?>" ></br>

								<label for="courriel">Votre adresse courriel:</label></br>
								<input type="email" name="courriel" id="courriel" size="40" maxlength="40" required readonly  value="<?php echo $donnees['EmailUsers']; ?>"  ></br>

								<label for="login">Votre login:</label></br>
								<input type="text" name="login" id="login" required  size="4" maxlength="4" value="<?php echo $donnees['LoginUsers']; ?>"/></br>

								<label for="mdp">Choisissez un mot de passe</label></br>
								<input type="password" name="mdp" id="mdp" size="60" maxlength="60"required value="<?php echo $donnees['MdpUsers']; ?>" ></br>

								<label for="mdp2">Confirmez votre mot de passe</label></br>
								<input type="password" name="mdp2" id="mdp2" size="60" maxlength="60" required value="<?php echo $donnees['MdpUsers']; ?>"></br>

								<input type="reset" value="Annuler"/>
								<input type="submit" value="Envoyer"/>

							</fieldset>
						</form>
					<?php

					$bdd = null;

				}
				catch (PDOException $e) {
					echo "Erreur !: " . $e->getMessage() . "<br />";
					die();
				}
		?>
	</body>
</html>
