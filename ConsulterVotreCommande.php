<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
		<link rel="stylesheet" href="css/bootstrap.min.css"/>

	<title>Consulter votre commande</title>
</head>
<body>
	<h3>Vous avez commandé: </h3>
	<?php
	$_SESSION['panier']=array();
	$_SESSION['panier']['idProduits']=array();
	$_SESSION['panier']['quantiteProduits']=array();
	$j =0;
	$somme=0;
		for ($i = 1; $i <= 10; $i++) {
			$ProNum = "ProNum" . $i;
			$qProNum = "qProdNum" . $i;

			if (isset($_POST[$ProNum])) {
				$idProduits = $_POST[$ProNum];
				$qProduits = $_POST[$qProNum];

				$_SESSION['panier']['idProduits'][$j] = $idProduits;
				$_SESSION['panier']['quantiteProduits'][$j] = $qProduits;

				try {
					$bdd = new PDO("mysql:host=localhost;dbname=site", "root", "");
					$bdd->query("SET NAMES 'utf8'");

					$reponse = $bdd->query("SELECT * FROM produits WHERE idProduits='$idProduits'");
					$donnees = $reponse->fetch();

					echo $donnees['descriptionProduits'];
					echo ", ";
					echo $donnees['prixProduits'];
					echo " CHF l'unité, en ";
					echo $qProduits;
					echo " exemplaire-s";
					echo "<br />";
					 $somme = $somme + $donnees['prixProduits'] * $qProduits;


					$bdd = NULL;; // Déconnexion de MySQL
				}
				catch (PDOException $e) {
					echo "Erreur !: " . $e->getMessage() . "<br />";
					die();
				}

				$j = $j + 1;
			}
		}
		echo "</br>";
		echo "Pour un montant total de ";
					echo $somme." CHF.</br></br>";
	?>
	<form action="EnregistrerVotreCommande.php" method="post">
		<input type="hidden" name="nombreProduits" value="<?php echo $j; ?>" />
		<input type="submit" value="Envoyer"/></br></br>
	</form>

	<a href="PasserUneCommande.php">Retour à la page présentant les articles pouvant être commandés...</a></br></br>
	<a href="Entree.php">Retour à la page d'accueil</a>

</body>
</html>
